﻿namespace API.Models
{
    namespace Backend.Models
    {
        public class Result
        {
            public bool IsSuccess { get; set; }
            public string FunctionMessage { get; set; }
        }
    }
}
